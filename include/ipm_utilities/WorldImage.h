/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldImage.h
 * Author: ariel
 *
 * Created on May 11, 2016, 1:27 PM
 */

#ifndef WORLDIMAGE_H
#define WORLDIMAGE_H

#include <string>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <cmath> 
#include "Camera.h"

using namespace std;
using namespace cv;

class WorldImage {
public:
    WorldImage();
    WorldImage(Mat &pix, Mat &weig);
    virtual ~WorldImage();
    
    void getWorldImage(Mat& img, Mat &ipm);
    void getWorldImagePRO(Mat& img, Mat &ipm);
    
private:
    
    int nRows;
    int nCols;
    Mat pixels;
    Mat weights;
    
    float getPixVal(Mat &img,int i, int j);

};

#endif /* WORLDIMAGE_H */

