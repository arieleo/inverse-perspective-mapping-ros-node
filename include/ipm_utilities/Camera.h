/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Camera.h
 * Author: ariel
 *
 * Created on May 5, 2016, 7:45 PM
 */

#ifndef CAMERA_H
#define CAMERA_H

#include <string>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <cmath> 

using namespace std;
using namespace cv;

class Camera {
public:
    Camera();
    Camera(int, int, float, float, float);
    //Camera(const Camera& orig);
    virtual ~Camera();
    
//    void setName(string);
    void setM(int);
    void setN(int);
    void setH(float);
    void setAlpha(float);
    void setTheta(float);
    void pixelsToWorld();
    void setElement(Mat &matr, int j, int i, float v);
    
//    string getName() {return name;}
    float getM() { return m;}
    float getH() { return h;}
    float getAlpha() { return alpha;}
    float getTheta() { return theta;}
    Mat getXM() {return xMap;}
    Mat getYM() {return yMap;}
    
    //cv::Mat getH() const { return m_H; }
    
    
private:
    
//    string name;
    int m;
    int n;
    float h;
    float alpha;
    float theta;
    Mat xMap;
    Mat yMap;
    float mc;
    
    void calcParams();
    void startMaps(Mat &m1, Mat &m2);
   

};

#endif /* CAMERA_H */

