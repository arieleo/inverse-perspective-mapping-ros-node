/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Params.h
 * Author: ariel
 *
 * Created on May 9, 2016, 2:53 PM
 */

#ifndef PARAMS_H
#define PARAMS_H

#include <string>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <cmath> 
#include "Camera.h"

using namespace std;
using namespace cv;

class Params {
public:
    Params();
    Params(Vec2f xr, Vec2f yr, int msz, Mat xM, Mat yM, Size mT, float mm);
//    Params(const Params& orig);
    virtual ~Params();
    
    Vec2f getXRange(){return xRange;}
    Vec2f getYRange(){return yRange;}
    int getMIPM(){return mIPM;}
    Mat getPixels(){return pixels;}
    Mat getWeights(){return weights;}
    float getStep(){return step;}
    
    void genIntMap();
    void returnInverse(Mat& img, Mat &dest, Mat &xMap, Mat &yMap);
    void convertToMeters(Size &sz, Mat &metX, Mat &metY);
    void convertMapsToPix(Size &zs, Mat &xMapP, Mat &yMapP);
    
private:
    
    vector<float> getVec(float st, float end, float ste);
    
    Mat xMap;
    Mat yMap;
    Size mc;
    Vec2f xRange;
    Vec2f yRange;
    int mIPM;
    float step;
    float m;
    Mat pixels;
    Mat weights;
    
    void calcStep();
    void setVecValues(Vec4f &v1, Vec2f &v2);
    void fillMat(Mat& mat, int rw, int cl, int ch, int v);
    int getVis(Mat & xvec, float x, float xmv);
    void getRowBounds(Vec2f &r_1234, int xrow, int mC);
    void getColBounds(Vec4f &cvec,float y,Mat &yMap,Vec2f &r_1234);
    void getIndex(int m, int mC, int n, Vec4f &rvec, Vec4f &cvec, Vec4f &ivec);
    int subToInd(int r, int c, int y, int x);
    void getWeights(float x,float y,Vec4f &rvec,Vec4f &cvec,Mat &xMap,Mat &yMap, Vec4f &wvec);
    int isMember(int c, float *cDisp);

};

#endif /* PARAMS_H */

