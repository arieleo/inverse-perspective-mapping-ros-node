This is a ROS-based library for Inverse Perspective Mapping image.

The develop of these is made in C++ based in the paper:

Bertozz, M., Broggi, A., & Fascioli, A. (1998). Stereo inverse perspective mapping: theory and applications. Image and vision computing, 16(8), 585-590.

The use of this generates a new transformed image of the perspective in front of a camera for Autonomous Driving applications in real-time.

Compile this library adding as a folder inside a work space of ROS.

FOR USING:

include in the code of ros node:

#include "ipm_utilities/Camera.h"
#include "ipm_utilities/Params.h"
#include "ipm_utilities/WorldImage.h"

Set parameters of a Camera object c as follows:

Camera c(height_original_image, width_original_image, height_of_camera, aperture_of_camera_rads, pitch_of_camera_rads);

pitch of the camera is positive when is pointing to downside. Get the perspective maps for new image in x and y coordinates:

Mat xMap = c.getXM();
Mat yMap = c.getYM();

Size mc = xMap.size();
float m = c.getM();

Vec2f xRange(0.0f, 1.4f);   // Range of the new image in meters for x
Vec2f yRange(-0.75f, 0.75f);// Range of the new image in meters for y
int mIPM = 400;             // height of the new image in pixels

Params p(xRange, yRange, mIPM, xMap, yMap, mc, m);    // Object for perspective transform

p.convertMapsToPix(sz, xMapP, yMapP);

Mat pixeles = p1.getPixels();           // Matrix of color pixels for transform
Mat weights = p1.getWeights();          // Matrix of color weights for image transformation

WorldImage wi(pixeles, weights);        // Object for get the new image.


For get new image in real time just use de following sentence in the code:

w.getWorldImagePRO(imgray, ipm);

where imgray is the original gray-scale image from vehicle camera and ipm is the new Mat object for save the perspective transformed image.

For any question feel free to email me.
Author: Ariel E. Ortiz Esquivel.
arieleo@ccc.inaoep.mx