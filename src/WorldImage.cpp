/**
 * @author Ariel Esau Ortiz Esquivel <arieleo@ccc.inaoep.mx>
 * @date June 2016
 */

#include "ipm_utilities/WorldImage.h"

WorldImage::WorldImage(Mat& pix, Mat& weig){
    Size sz= pix.size();
    nRows= sz.height;
    nCols= sz.width;
    pixels= pix.clone();
    weights= weig.clone();
}

WorldImage::WorldImage(){
}

WorldImage::~WorldImage() {
}

float WorldImage::getPixVal(Mat &img,int i, int j){
    Vec4f aux(0,0,0,0);
    for (int ii=0; ii<4; ii++){
        aux.val[ii]= img.at<float>(pixels.at<Vec4f>(i,j)[ii]);
    }
    float val= sum(aux.mul(weights.at<Vec4f>(i,j))).val[0];
//    cout<<sum(aux.mul(weights.at<Vec4f>(i,j)))<<endl;
    return val;
}

void WorldImage::getWorldImage(Mat& img, Mat &ipm){
    Mat img2;
    img.convertTo(img2, CV_32F);
    img2= img2/255;
//    cout<<img2.at<float>(300,250)<<endl;
//    cout<<img2.at<float>((300*640)+250)<<endl;
    Mat wimg(nRows, nCols, CV_32F);
    
    for (int i=0; i<nRows; i++){
        for (int j=0; j<nCols; j++){
            float px= getPixVal(img2,i,j);
            if (px>1)
                px=1;
            if (px<0)
                px=0;
            wimg.at<float>(i,j)= px;
        }
    }
    wimg= wimg*255;
    wimg.convertTo(ipm, CV_8U);
}

void WorldImage::getWorldImagePRO(Mat& img0, Mat &ipm){
//    Mat img= Mat::zeros(img0.size(), CV_32F);
//    img0.copyTo(img);
    Mat img;
    img0.convertTo(img, CV_32F);
    Mat wimg(nRows, nCols, CV_32F);
    
    Vec4f *datapix= pixels.ptr<Vec4f>();
    Vec4f *datawei= weights.ptr<Vec4f>();
    float *wimgPT= wimg.ptr<float>();
    float *dataimg= img.ptr<float>();
    Vec4f val;
    Vec4f vecimg;
    
    int cont= nRows*nCols;
    float valp;
    
    for (int ii=0; ii<cont; ii++){
        vecimg= datapix[ii];
        val=Vec4f(dataimg[(int)vecimg.val[0]],dataimg[(int)vecimg.val[1]],dataimg[(int)vecimg.val[2]],dataimg[(int)vecimg.val[3]]);
        valp= sum(val.mul(datawei[ii])).val[0];
        if(valp<0)
            wimgPT[ii]= 0;
        else if(valp>255)
            wimgPT[ii]=255;
        else
            wimgPT[ii]=valp;
    }
    wimg.convertTo(ipm,CV_8U);
    
}
