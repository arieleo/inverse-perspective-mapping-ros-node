/**
 * @author Ariel Esau Ortiz Esquivel <arieleo@ccc.inaoep.mx>
 * @date June 2016
 */

#include "ipm_utilities/Camera.h"

Camera::Camera(int m, int n, float h, float al, float th) {
//    this->name= name;
    this->m= m;
    this->n= n;
    this->h= h;
    this->alpha=al;
    this->theta=th;
    calcParams();
}

Camera::Camera(){
    
}

Camera::~Camera() {
}

void Camera::calcParams(){
    float den = sqrt(pow(m-1,2)+pow(n-1,2));
    float u= atan((n-1)/den*tan(alpha));
    float v= atan((m-1)/den*tan(alpha));
    float rHor= ceil((m-1)/2*(1-tan(theta)/tan(v))+1)+ceil(m*0.05);
    
    mc= m-rHor+1;
    
    xMap= Mat::zeros(mc, n, CV_32F);
    yMap= Mat::zeros(mc, n, CV_32F);
    
    //filas mc , columans n
    
    float r0, rfac, num, d, num2, d2;
    for(int j=0; j<mc; j++){
        r0= j+rHor;
        rfac= (1- (2*(r0-1))/(float)(m-1))*tan(v);
        num= 1+(rfac*tan(theta));
        d= tan(theta)-rfac;
        float *data1= xMap.ptr<float>(j);
        float *data2= yMap.ptr<float>(j);
        for(int i=0; i<n; i++){
            data1[i]= h*(num/d);
            num2= (1-2*(float)i/(float)(n-1))*tan(u);
            d2= sin(theta)-rfac*cos(theta);
            data2[i]= h*(num2/d2);
        }
    }
}

//void Camera::calcParams(){
//    float den = sqrt(pow(m-1,2)+pow(n-1,2));
//    float u= atan((n-1)/den*tan(alpha));
//    float v= atan((m-1)/den*tan(alpha));
//    float rHor= ceil((m-1)/2*(1-tan(theta)/tan(v))+1)+ceil(m*0.05);
//    
//    mc= m-rHor+1;
//    
//    xMap= Mat::zeros(mc, n, CV_32F);
//    yMap= Mat::zeros(mc, n, CV_32F);
//    
//    for (int ii=0; ii<mc; ii=ii+1){
//        float r0= ii+rHor;
//        float rfac= (1- (2*(r0-1))/(float)(m-1))*tan(v);
//        
//        float num= 1+(rfac*tan(theta));
//        float d= tan(theta)-rfac;
//        
//        for (int ix=0; ix<n; ix=ix+1){
//            setElement(xMap,ii,ix,h*(num/d));
//        }
//        
//        for (int c=0; c<n; c=c+1){
//            float num2= (1-2*(float)c/(float)(n-1))*tan(u);
//            float d2= sin(theta)-rfac*cos(theta);
//            setElement(yMap,ii,c,h*(num2/d2));
//        }
//    }
//}

void Camera::setElement(Mat& matr, int j, int i, float v){
    matr.at<float>(j,i)=v;
}
