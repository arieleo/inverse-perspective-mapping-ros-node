/**
 * @author Ariel Esau Ortiz Esquivel <arieleo@ccc.inaoep.mx>
 * @date June 2016
 */

#include "ipm_utilities/Params.h"

Params::Params(Vec2f xr, Vec2f yr, int msz, Mat xM, Mat yM, Size mT, float mm) {
    xRange = xr;
    yRange = yr;
    mIPM = msz;
    xMap = xM;
    yMap = yM;
    mc = mT;
    m = mm;
    calcStep();
    genIntMap();
}

Params::Params() {
}

void Params::convertToMeters(Size &sz, Mat &metX, Mat &metY) {
    metX = Mat::zeros(sz, CV_32F);
    metY = Mat::zeros(sz, CV_32F);

    for (int ii = 0; ii < sz.height; ii++) {
        float yVal = (((yRange.val[1] - yRange.val[0]) / (float) sz.height) * ii) + yRange.val[0];
        for (int jj = 0; jj < sz.width; jj++) {
            float xVal = (((xRange.val[1] - xRange.val[0]) / (float) sz.width) * jj) + xRange.val[0];
            metX.at<float>(ii, jj) = xVal;
            metY.at<float>(ii, jj) = yVal;
        }
    }
}

void Params::convertMapsToPix(Size &sz, Mat& xMapP, Mat& yMapP) {
    xMapP = Mat::zeros(xMap.size(), CV_32F);
    yMapP = Mat::zeros(yMap.size(), CV_32F);

    for (int ii = 0; ii < xMap.rows; ii++) {
        for (int jj = 0; jj < xMap.cols; jj++) {
            float yVal = sz.height-(sz.height / (yRange.val[1] - yRange.val[0]))*(yMap.at<float>(ii, jj) - yRange.val[0]);
            float xVal = (sz.width / (xRange.val[1] - xRange.val[0]))*(xMap.at<float>(ii, jj) - xRange.val[0]);
            xMapP.at<float>(ii, jj) = xVal;
            yMapP.at<float>(ii, jj) = yVal;
        }
    }
}

void Params::returnInverse(Mat& img, Mat &dest, Mat &xMap, Mat &yMap) {
    cv::remap(img, dest, xMap, yMap, INTER_NEAREST);
            //    INTER_NEAREST - a nearest-neighbor interpolation
            //INTER_LINEAR - a bilinear interpolation (used by default)
            //INTER_AREA - resampling using pixel area relation. It may be a preferred method for image decimation, as it gives moire’-free results. But when the image is zoomed, it is similar to the INTER_NEAREST method.
            //INTER_CUBIC - a bicubic interpolation over 4x4 pixel neighborhood
            //INTER_LANCZOS4 - a Lanczos interpolation over 8x8 pixel neighborhood
}

void Params::calcStep() {
    step = (yRange.val[1] - yRange.val[0]) / (mIPM - 1);
    //    cout<< step;
}

vector<float> Params::getVec(float st, float end, float ste) {
    int num = 1 + abs(end - st) / abs(ste);
    //    cout<< num<<endl;
    vector<float> vec;
    for (int i = 0; i < num; i++) {
        vec.push_back(st + (ste * i));
    }

    return vec;
}

void Params::fillMat(Mat& mat, int rw, int cl, int ch, int v) {
    for (int i = 0; i < rw; i++) {
        Vec4f *data = mat.ptr<Vec4f>(i);
        Vec4f vec(v, v, v, v);
        for (int j = 0; j < cl; j++) {
            data[j] = vec;
        }
    }
}

//void Params::fillMat(Mat& mat, int rw, int cl, int ch, int v){
//    for (int i=0; i<rw; i++){
//        for(int j=0; j<cl; j++){
//            for(int k=0; k<ch; k++){
//                mat.at<Vec4f>(i,j)[k]=v;
//            }
//        }
//    }
//}

int Params::getVis(Mat & xvec, float x, float xmv) {
    int sum = 0;
    int s1 = xvec.rows;
    int s2 = xvec.cols;
    //Mat aux= Mat::zeros(s, 1, CV_8U);
    if (x >= xmv) {
        if (s1 > s2) {
            for (int i = 0; i < s1; i++) {
                if (xvec.at<float>(i, 1) >= x)
                    sum = sum + (1);
            }
        } else {
            for (int i = 0; i < s2; i++) {
                if (xvec.at<float>(1, i) >= x)
                    sum = sum + (1);
            }
        }
    }
    //    cout<<sum<<endl;
    return sum;
}

void Params::getRowBounds(Vec2f& r_1234, int xrow, int mC) {
    if (xrow < mC) {
        r_1234.val[0] = xrow;
        r_1234.val[1] = xrow - 1;
    } else {
        r_1234.val[0] = xrow - 1;
        r_1234.val[1] = xrow - 2;
    }
}

void Params::getColBounds(Vec4f& cvec, float y, Mat& yMap, Vec2f& r_1234) {
    Mat yvec12 = yMap.row(r_1234.val[0]);
    int ycol12 = getVis(yvec12, y, yvec12.at<float>(yvec12.cols - 1));
    if (ycol12 >= 0) {
        Mat yvec34 = yMap.row(r_1234.val[1]);
        int ycol34 = getVis(yvec34, y, yvec34.at<float>(yvec34.cols - 1));
        if (ycol34 >= 0) {
            if (ycol12 <= yvec12.cols) {
                cvec.val[0] = ycol12 - 1;
                cvec.val[1] = ycol12;
            } else {
                cvec.val[0] = ycol12 - 2;
                cvec.val[1] = ycol12 - 1;
            }
            if (ycol34 <= yvec34.cols) {
                cvec.val[2] = ycol34 - 1;
                cvec.val[3] = ycol34;
            } else {
                cvec.val[2] = ycol34 - 2;
                cvec.val[3] = ycol34 - 1;
            }
        }
    }

}

int Params::subToInd(int rows, int cols, int row, int col) {
    return (row * cols)+col;
}

void Params::getIndex(int m, int mC, int n, Vec4f& rvec, Vec4f& cvec, Vec4f& ivec) {
    int sh = m - mC;
    Vec4f rorig(rvec.val[0] + sh, rvec.val[1] + sh, rvec.val[2] + sh, rvec.val[3] + sh);
    for (int i = 0; i < 4; i++) {
        ivec.val[i] = subToInd(m, n, rorig.val[i], cvec.val[i]);
    }
}

void Params::getWeights(float x, float y, Vec4f &rvec, Vec4f &cvec, Mat &xMap, Mat &yMap, Vec4f &wvec) {
    //int DEBUG=1;
    Vec4f px(0, 0, 0, 0);
    Vec4f py(0, 0, 0, 0);
    for (int p = 0; p < 4; p++) {
        px.val[p] = xMap.at<float>(rvec.val[p], cvec.val[p]);
        py.val[p] = yMap.at<float>(rvec.val[p], cvec.val[p]);
    }

    float d12y = py.val[0] - py.val[1];
    float d1y = py.val[0] - y;
    float d2y = y - py.val[1];

    float d34y = py.val[2] - py.val[3];
    float d3y = py.val[2] - y;
    float d4y = y - py.val[3];

    float d13x = px.val[2] - px.val[0];
    float d3x = px.val[2] - x;
    float d1x = x - px[0];

    wvec.val[0] = d3x * d2y / (d13x * d12y);
    wvec.val[1] = d3x * d1y / (d13x * d12y);
    wvec.val[2] = d1x * d4y / (d13x * d34y);
    wvec.val[3] = d1x * d3y / (d13x * d34y);

}

void Params::setVecValues(Vec4f& v1, Vec2f& v2) {
    v1.val[0] = v2.val[0];
    v1.val[1] = v2.val[0];
    v1.val[2] = v2.val[1];
    v1.val[3] = v2.val[1];
}

void Params::genIntMap() {

    int n = mc.width;
    int mC = mc.height;

    //    cout<<m<<endl;
    //    cout<<n<<endl;
    //    cout<<mC<<endl;

    vector <float> xG = getVec(xRange.val[0], xRange.val[1], step);
    vector <float> yG = getVec(yRange.val[1], yRange.val[0], -step);

    Mat xGrid(xG);
    Mat yGrid(yG);

    int nRows = yGrid.rows;
    int nCols = xGrid.rows;

    const int sz[] = {nRows, nCols, 4};
    pixels.create(3, sz, CV_32F);
    weights.create(3, sz, CV_32F);

    fillMat(pixels, nRows, nCols, 4, 1);
    fillMat(weights, nRows, nCols, 4, 0);

    Mat xVec = xMap.col(0);

    float xMinVis = xVec.at<float>(xVec.rows - 1);
    float milestones[10];
    float cDisp[10];
    float pcnt[10];
    for (int ml = 0; ml < 10; ml++) {
        milestones[ml] = (float) nCols / 10 * (ml + 1);
        cDisp[ml] = floor(milestones[ml]);
        pcnt[ml] = 10 * (ml + 1);
    }

    Vec2f r_1234(0.0f, 0.0f);
    Vec4f cvec(0, 0, 0, 0);
    Vec4f wvec(0, 0, 0, 0);
    Vec4f ivec(0, 0, 0, 0);
    Vec4f rvec(0, 0, 0, 0);

    float x, y;
    float *dataX = xGrid.ptr<float>();
    float *dataY = yGrid.ptr<float>();
    Vec4f *dataPix = pixels.ptr<Vec4f>();
    Vec4f *dataWei = weights.ptr<Vec4f>();

    for (int c = 0; c < nCols; c++) {
        x = dataX[c];
        int xrow = getVis(xVec, x, xMinVis);

        if (xrow > 0) {

            getRowBounds(r_1234, xrow, mC);
            for (int r = 0; r < nRows; r++) {
                y = dataY[r];
                getColBounds(cvec, y, yMap, r_1234);
                if (cvec.val[0] >= 0) {
                    setVecValues(rvec, r_1234);
                    getIndex(m, mC, n, rvec, cvec, ivec);
                    getWeights(x, y, rvec, cvec, xMap, yMap, wvec);

                    dataPix[(r * nCols) + c] = ivec;
                    dataWei[(r * nCols) + c] = wvec;
                }
            }

        }
    }
}

int Params::isMember(int c, float *cDisp) {
    bool res = false;
    int index = -1;
    for (int i = 0; i < 10; i++) {
        int com = (int) cDisp[i];
        //        cout<<(int)cDisp[i]<<endl;
        if (c == com) {
            res = true;
            index = i;
        }
    }
    return index;
}

Params::~Params() {
}

